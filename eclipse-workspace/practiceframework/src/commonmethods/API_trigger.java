package commonmethods;

import environmentandrepo.request;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_trigger extends request {
	
	static String headername = "Content-Type";
	static String headervalue = "application/json";

	public static Response postapi(String request, String endpoint) {

		RequestSpecification rs = RestAssured.given();
		rs.header(headername, headervalue);
		rs.body(request);
		Response response = rs.post(endpoint);
		return response;
	}
		
		
	}


