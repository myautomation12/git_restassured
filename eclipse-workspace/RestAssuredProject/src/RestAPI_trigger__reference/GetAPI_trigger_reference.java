package RestAPI_trigger__reference;

import io.restassured.RestAssured;
import org.testng.Assert;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

public class GetAPI_trigger_reference {

	public static void main(String[] args) {
	
		 String hostname = "https://reqres.in/";
		 String resource = "/api/users?page=2";
		 
		 
		    int[] id = {7,8,9,10,11,12};
			String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
			String[] first_name= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
			String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
			String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
			
			
		 
		 RestAssured.baseURI= hostname;
		 
     String		Responsebody =  given().when().get(resource).then().log().all().extract().response().asString();
          
		   System.out.println(Responsebody);
		   
		         JsonPath json_res = new JsonPath(Responsebody);
		     
		         int count =  json_res.getInt("data.size()");
		         System.out.println("count:" + count);
				
		    
				for (int i=0; i< count; i++)
				{
					String exp_id = Integer.toString(id[i]);
					
					System.out.println(exp_id);
					
					String exp_email = email[i];
					System.out.println(exp_email);
					
					String exp_firstname = first_name[i];
				    String exp_lastname = last_name[i];
				    String exp_avatar = avatar[i];
					//System.out.println("jso_res "+json_res.getString(Responsebody);
					String res_id= json_res.getString("data["+i+"].id");
					System.out.println(res_id);
					String res_email= json_res.getString("data["+i+"].email");
					System.out.println(res_email);
					
					String res_firstname= json_res.getString("data["+i+"].first_name");
					String res_lastname= json_res.getString("data["+i+"].last_name");
					String res_avatar= json_res.getString("data["+i+"].avatar");
					
					Assert.assertEquals(res_email, exp_email);
					Assert.assertEquals(res_id, exp_id);
					
					Assert.assertEquals(res_firstname, exp_firstname);
					Assert.assertEquals(res_lastname, exp_lastname);
					Assert.assertEquals(res_avatar, exp_avatar);
					
				}
				
			
		    
		   
		    
		   
		
		  
		  
		     
		       
	}

	
}
