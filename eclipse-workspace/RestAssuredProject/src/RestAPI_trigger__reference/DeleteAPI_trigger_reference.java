package RestAPI_trigger__reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;
public class DeleteAPI_trigger_reference {

	public static void main(String[] args) {
		   
		 String hostname = "https://reqres.in/";
		 String resource = "/api/users/2";
		 
		 RestAssured.baseURI = hostname; 
		 
		String responsebody =  given().log().all().when().delete(resource).then().log().all().extract().asString();
		System.out.println(responsebody);
		  
		  JsonPath json_res = new JsonPath(responsebody);
		      
		

	}

}
