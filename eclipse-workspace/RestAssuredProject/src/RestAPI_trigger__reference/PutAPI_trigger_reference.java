package RestAPI_trigger__reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class PutAPI_trigger_reference {

	public static void main(String[] args) {

		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n" + "    \"name\": \"Roshni\",\r\n" + "    \"job\": \"Tester\"\r\n" + "}";

		// Configure the API and trigger
		RestAssured.baseURI = hostname;

		String responseBody = given().header(headername, headervalue).body(requestBody).when().put(resource).then()
				.extract().response().asString();

		System.out.println(responseBody);

		// Parse the response Body
		// Create the object of Json path

		JsonPath jsp_res = new JsonPath(responseBody);

		// Parse individual parameters using jsp_res object

		String res_name = jsp_res.getString("name");
		System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		System.out.println(res_job);

		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		System.out.println(res_updatedAt);

		// Validate the response body

		// Parse the request body and save into local variable

		JsonPath jsp_req = new JsonPath(requestBody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Generate and slice the date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Use TestNG assertion

		Assert.assertEquals(res_name, req_name, "Name is validated");
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}

}
