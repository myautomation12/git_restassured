package RestAPI_trigger__reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class PostAPI_trigger_reference {

	public static void main(String[] args) {

		String hostname = "https://reqres.in/";
    	String resource = "/api/users";
		String headername = "contenttype";
		String headervalue = "application/json";
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

	RestAssured.baseURI = "hostname";

		String Responsebody = given().header(headername, headervalue).body(requestbody).log().all().when()
				.post(RestAssured.baseURI).then().log().all().extract().response().asString();
		
		

		System.out.println(Responsebody);
		JsonPath json_res = new JsonPath(Responsebody);
		String res_name = json_res.getString("name");
		
		

		System.out.println(res_name);
		String res_job = json_res.getString("job");
	
		String res_id = json_res.getString("id");

		String res_createdAt = json_res.getString("createdAt");
	
		res_createdAt = res_createdAt.substring(0, 11);

		JsonPath json_req = new JsonPath(requestbody);

		String req_name = json_req.getString("name");
		System.out.println(req_name);
		String req_job = json_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);

		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}

}
