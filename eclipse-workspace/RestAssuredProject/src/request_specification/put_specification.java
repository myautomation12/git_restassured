package request_specification;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class put_specification {

	public static void main(String[] args) {
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n" + "    \"name\": \"praveen\",\r\n" + "    \"job\": \"Tester\"\r\n" + "}";
		
		
	    RequestSpecification rs = RestAssured.given();

		
		rs.header(headername, headervalue);

		
	    rs.body(requestBody);

		// Step 2.4 : Trigger the API
		Response response = rs.put(hostname + resource);

		// Step 3: Extract the status code
		int statuscode = response.statusCode();
		System.out.println(statuscode);

		// Step 4: Fetch the response body parameters
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		
		String updatedAt = responseBody.jsonPath().getString("updatedAt");
		updatedAt = updatedAt.toString().substring(0, 11);

		// Step 5: Fetch the request body parameters

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6 : Generate expected data.

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 7 : Validate using TestNG Assertions

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(updatedAt, expecteddate, "updatedAt in ResponseBody is not equal to Date Generated");

		
		
		
		
		
		

	}

}
