package request_specification;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class req_spec_post_practice {

	public static void main(String[] args) {
	
		
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		
		     RequestSpecification rs = RestAssured.given();
		     
		     rs.header(headername, headervalue);
		     rs.body(requestBody);
		   Response response = rs.post(hostname+resource);
		   
		  int Statuscode = response.statusCode();
		  
		  ResponseBody responsebody = response.getBody();
		     responsebody.jsonPath().getString("name");
		  
	}

}
