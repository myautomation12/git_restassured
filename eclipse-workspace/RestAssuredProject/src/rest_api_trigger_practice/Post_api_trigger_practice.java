package rest_api_trigger_practice;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_api_trigger_practice {

	public static void main(String[] args) {

		// step1
		String headername = "Content-Type";

		String headervalue = "application/json";
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		// step2
		RestAssured.baseURI = hostname;

		// step3

		// given().header(headername,
		// headervalue).body(requestbody).log().all().when().post(resource).then().log().all().extract().response().asString();

		// step4
		String Responsebody = given().header(headername, headervalue).body(requestbody).when().post(resource).then()
				.extract().response().asString();

		System.out.println(Responsebody);

		// step5

		JsonPath jp = new JsonPath(Responsebody);

		String res_name = jp.getString("name");

		System.out.println(res_name);

		String res_job = jp.getString("job");
		System.out.println(res_job);
		String res_id = jp.getString("id");
		
		String res_createdat = jp.getString("createdAt");
		
		res_createdat = res_createdat.substring(0,11); 
		
		  JsonPath jpreq = new JsonPath(requestbody);
		  
		String req_name=   jpreq.getString("name");
		String  req_job=  jpreq.getString("job");
		

		LocalDateTime currentdate = LocalDateTime.now();
	String	expecteddate = currentdate.toString().substring(0, 11);
		
	
	Assert.assertEquals(res_name, req_name);
	Assert.assertEquals(res_job, req_job);
	Assert.assertNotNull(res_id);
	Assert.assertEquals(res_createdat, expecteddate);
	
	

	}

}
