package rest_api_trigger_practice;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Get_api_trigger_practice {


	public static void main(String[] args) {
		 int exp_page = 2;
			int per_page = 6;
			int total = 12;
			int total_pages = 2;
			String exp_url = "https://reqres.in/#support-heading";
			String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

			
		String hostname = "https://reqres.in/";
		String resource = "api/users?page=2";

		RestAssured.baseURI = hostname;

		String responsebody = given().when().get(resource).then().extract().response().asString();

		System.out.println(responsebody);

		JsonPath jp = new JsonPath(responsebody);

		int res_page = jp.getInt("page");
		int res_perpage = jp.getInt("per_page");
		int res_totalpages = jp.getInt("total_pages");
		int  res_total = jp.getInt("total");
		String res_support_url = jp.getString("support.url");
		String res_support_text = jp.getString("support.text");

		
		Assert.assertEquals(exp_page, res_page );
		Assert.assertEquals(res_total, total);
       Assert.assertEquals(res_totalpages, total_pages);
       Assert.assertEquals(res_perpage,per_page);
       Assert.assertEquals(res_support_url,exp_url);
       Assert.assertEquals(res_support_text,exp_text);
       
	}

}
