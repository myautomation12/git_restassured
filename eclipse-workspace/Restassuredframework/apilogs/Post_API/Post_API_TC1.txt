Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Fri, 03 May 2024 15:27:10 GMT
Content-Type=application/json; charset=utf-8
Content-Length=84
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714750030&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=g7YGeYIi1HBoSnp%2FS0rb1E1Y5OSsr6qrR4L7CQASjz4%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714750030&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=g7YGeYIi1HBoSnp%2FS0rb1E1Y5OSsr6qrR4L7CQASjz4%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"54-3b7o8Jf7ovbppS8cZxziCYP1894"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87e14609bf252e8d-HYD

Response body is :
{"name":"morpheus","job":"leader","id":"629","createdAt":"2024-05-03T15:27:10.600Z"}