package environmentandrepository;

import java.io.IOException;
import java.util.ArrayList;

import commonmethods.Utilities;

public class Requestrepository extends Environment {

	public static String post_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return requestBody;
	}
	
	public static String post_param_requestBody(String TestcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExceldata("Post_api", TestcaseName);
		String name = data.get(1);
		String Job = data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \""+ name +"\",\r\n" + "    \"job\": \""+Job+"\"\r\n" + "}";
		return requestBody;
	}

	public static String put_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"praveen\",\r\n" + "    \"job\": \"Tester\"\r\n" + "}";
		return requestBody;
		
	}

	public static String patch_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"Praveen\",\r\n" + "    \"job\": \"Tester\"\r\n" + "}";
		return requestBody;
	}

	public static String delete_request_body() {
		String requestBody = "";

		return requestBody;
	}

	public static String get_request_body() {
		String requestBody = "";

		return requestBody;
	}
}
