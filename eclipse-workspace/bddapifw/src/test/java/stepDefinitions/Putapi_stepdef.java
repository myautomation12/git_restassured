package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import commonmethods.API_Trigger;
import environmentandrepository.Environment;
import environmentandrepository.Requestrepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Putapi_stepdef {
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responsebody;

	@Given("the API endpoint with {string} and {string}")
	public void the_api_endpoint_with_and(String string, String string2) {
		endpoint = Environment.put_endpoint();
		requestbody = "{\r\n" + "    \"name\": \"" + string + "\",\r\n" + "    \"job\": \"" + string2 + "\"\r\n" + "}";
	}

	@Given("the API endpoint with NAME and JOB")
	public void the_api_endpoint_with_name_and_job() {

		endpoint = Environment.put_endpoint();
		requestbody = Requestrepository.put_request_body();
	}

	@When("I send a PUT request with the following JSON  payload")
	public void i_send_a_put_request_with_the_following_json_payload() {
		response = API_Trigger.Put_API_Trigger(requestbody, endpoint);
	}

	@Then("the response status code should be {int}")
	public void the_response_status_code_should_be(Integer int1) {

		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

	}

	@Then("the response body should contain the updated")
	public void the_response_body_should_contain_the_updated() {
		responsebody = response.getBody();
		String res_name = responsebody.jsonPath().getString("name");
		String res_job = responsebody.jsonPath().getString("job");

		String res_createdAt = responsebody.jsonPath().getString("updatedAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");

		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

}
