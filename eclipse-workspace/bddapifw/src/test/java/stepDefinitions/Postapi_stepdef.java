package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import commonmethods.API_Trigger;
import environmentandrepository.Environment;
import environmentandrepository.Requestrepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Postapi_stepdef {
	String requestbody;
	String endpoint;
    Response response;
    ResponseBody responsebody;
	@Given("Enter NAME and JOB in requestbody")
	public void enter_name_and_job_in_requestbody() {
		 endpoint =Environment.post_endpoint();
	 requestbody = Requestrepository.post_request_body();
	    
	   
	}
	@Given("Enter {string} and {string} in requestbody")
	public void enter_and_in_requestbody(String string, String string2) {
		 endpoint =Environment.post_endpoint();
		
		 requestbody = "{\r\n"
					+ "    \"name\": \""+string+"\",\r\n"
					+ "    \"job\": \""+string2+"\"\r\n"
					+ "}";
	  
	}
	@When("send the request with payload")
	public void send_the_request_with_payload() {
		
	response = API_Trigger.Post_API_Trigger(requestbody, endpoint);
	   
	    
	}
	@Then("validate statuscode")
	public void validate_statuscode() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
	    
	   
	}
	@Then("validate responsebody parameters")
	public void validate_responsebody_parameters() {
		responsebody = response.getBody();
		String res_name = responsebody.jsonPath().getString("name");
		String res_job = responsebody.jsonPath().getString("job");
		String res_id = responsebody.jsonPath().getString("id");
		String res_createdAt = responsebody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	   
	    
	}

}
