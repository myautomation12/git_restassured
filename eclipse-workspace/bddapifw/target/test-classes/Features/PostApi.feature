Feature: Trigger the post api with requried parameters 
@PostApi
Scenario: Trigger the api request with valid requestbody  parameters

Given Enter NAME and JOB in requestbody 
When send the request with payload 
Then validate statuscode 
And validate responsebody parameters
@PostApi
 Scenario Outline:  Test Post api with multiple dataset
 Given Enter "<NAME>" and "<JOB>" in requestbody 
When send the request with payload 
Then validate statuscode 
And validate responsebody parameters
Examples: 
    |NAME|JOB|
    |praveen|SdeT|
    |aruna|topper|
    |ashwini|topper|
    

