Feature: Trigger the Patch api with change in  requried parameters 


@PatchApi 
Scenario: Update user information from request body
	Given  API endpoint with NAME and JOB 
	When I send a PATCH request with the following JSON  payload 
	Then response status code should be 200 
	And the response body should contain the updated parameters
@PatchApi 
Scenario Outline: Update user's information  from request body
	Given  API endpoint with "<NAME>" and "<JOB>" 
	When I send a PATCH request with the following JSON  payload 
	Then  response status code should be 200 
	And the response body should contain the updated parameters
	
	
	Examples: 
		|NAME|JOB|
		|praveen|SdeT|
		|aruna|topper|
		|ashwini|topper|
    