package testscript;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonmethods.api_trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class postscript extends api_trigger {
@Test
	public static void postvalidate() {
		
		Response response = postapi(post_requestbody(), post_endpoint() );{
			
			int status = response.statusCode();
			 ResponseBody responsebody = response.getBody();
			 
			String  res_name = responsebody.jsonPath().getString("name");
			String res_job = responsebody.jsonPath().getString("job");
			
		JsonPath rs = new JsonPath(post_requestbody());
		
		String  req_name = rs.getString("name");
		String  req_job = rs.getString("job");
			
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		
		}
	}
	 
}
