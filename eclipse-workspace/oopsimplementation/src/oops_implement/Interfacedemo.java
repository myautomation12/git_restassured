package oops_implement;


public class Interfacedemo implements student, student1 {

	public static void main(String[] args) {

		
		Interfacedemo obj = new Interfacedemo();
		
		obj.visa();
		obj.workvisa();
		obj.pancard();
		
		
		student obj1 = new Interfacedemo();
		 obj1.visa();
		 obj1.passport();
		 
		 student1 obj2 = new Interfacedemo();
		 
		 obj2.workvisa();
		
		
	}

	@Override
	public void workvisa() {
		System.out.println("work visa needed");
	}

	@Override
	public void visa() {
		System.out.println("visa required");

	}

	@Override
	public void passport() {
		System.out.println("passport requried");

	}

	public void pancard() {
		System.out.println("pancard");
	}

}







