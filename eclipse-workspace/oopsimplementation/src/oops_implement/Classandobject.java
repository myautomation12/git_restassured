package oops_implement;

class bike {

	String model;
	int cost;
	String color;

	public void Accelerate() {
		System.out.println(model + "bike started ");
	}

	public void Brake() {
		System.out.println(model + "bike stopped");
	}

	public void bikeDetails() {
		System.out.println("bike model " + model);
		System.out.println("bike cost " + cost);
		System.out.println("bike color " + color);

	}

}

public class Classandobject {

	public static void main(String[] args) {

		bike jupiter = new bike();

		jupiter.model = "jsp700";
		jupiter.cost = 800000;
		jupiter.color = "Red";
		jupiter.Accelerate();
		jupiter.Brake();
		jupiter.bikeDetails();

	}

}
