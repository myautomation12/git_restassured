package oops_implement;

class car {

	public void carDetails(int a) {

		System.out.println("carDetails method with one integer type parameter is called:" + a);

	}

	public void carDetails(int a, int b) {

		System.out.println("carDetails method with two integer parameters is called:" + a + "&" + b);

	}

	public void carDetails(String a, int b) {

		System.out.println("carDetails method with a String and integer parameter are called:" + a + "&" + b);

	}

	public static void main(String[] args) {

		car obj = new car();

		obj.carDetails(5);

		obj.carDetails(5, 10);
		obj.carDetails("Benz", 9);

	}

}
