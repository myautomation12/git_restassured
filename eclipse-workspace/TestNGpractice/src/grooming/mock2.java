package grooming;
import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class mock2 {
	
	@Test (priority= 2)
	public void mockapitrigger() {
		
		String hostname = "https://reqres.in/";
		String resource = "api/users";
		String headername = "Content-type";
		String headervalue = "application/json";
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
	RequestSpecification rs = RestAssured.given();
	
	rs.header(headername, headervalue);
	rs.body(requestbody);
	 Response response = rs.post(hostname+resource);
	 
	 int status = response.statusCode();	
	  ResponseBody responsebody = response.getBody();
	  
	  responsebody.jsonPath().getString("name");
	  
	  
Assert.assertEquals(status, 201);
		
		
		
	}
	
	@Test(priority =1)

	public void endpoint() {
	
		String hostname ="https://reqres.in/";
		String resource="api/users";
		String headername="Content-Type";
		String headervalue="application/json";
		String requestbody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";	
		
		 RequestSpecification rs = RestAssured.given();
		 
				rs.header(headername,headervalue);
				rs.body(requestbody);
				Response response = rs.post(hostname+resource);
				
			int status = 	response.statusCode();		
		   ResponseBody responsebody = response.getBody();
		   String res_name = responsebody.jsonPath().get("name");
		   
			JsonPath jsp_req = new JsonPath(requestbody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");

		   
		   Assert.assertEquals(res_name, req_name);
	}

}
