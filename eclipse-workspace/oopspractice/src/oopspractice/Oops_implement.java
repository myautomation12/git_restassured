package oopspractice;

public class Oops_implement {

	 private String name;
	 private int salary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	void display() {
		System.out.println("no parameters given");
	}

	void display(String name, int salary) {
		System.out.println(name + salary);
	}

	static class overiding extends Oops_implement {
		void display(int salary, String name) {
			System.out.println(salary + name);
		}
	}

	public static void main(String[] args) {

		overiding rs = new overiding();
		String name = "praveen";
		int salary = 1000;

		rs.display();

		
		rs.display(salary, name);
		rs.display(name, salary);
		rs.setName(name);
		System.out.println(rs.getName());
		

	}

}
