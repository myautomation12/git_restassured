package oopspractice;

public class Oops {
	
	private String name;
	private String surname;
	
	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	void display() {
		
		System.out.println("no params given");
	}
	
	void display (String name ,String surname) {
		 
		System.out.println(name + surname);
	}
static class methodoverriding extends Oops{
	
	void display(String surname , String name) {
		
		System.out.println(surname + name);
	}
	
}


	public static void main(String[] args) {
		
	methodoverriding rs = new methodoverriding();
	String name = "praveen";
	String surname = "gaddam";
	rs.display();
	rs.setSurname(surname);
	System.out.println(rs.getSurname());

	rs.setName(name);
	System.out.println(rs.getName());
	rs.display(name, surname);
	rs.display(surname, name);
		
	

	}

}
