Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "praveen",
    "job": "Tester"
}

Response header is :
Date=Tue, 07 May 2024 09:59:20 GMT
Content-Length=0
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1715075960&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=GSNTpUKO5TRSJXbwR0JCtwR7n6gFFMK7UGp4GhOoo%2BY%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1715075960&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=GSNTpUKO5TRSJXbwR0JCtwR7n6gFFMK7UGp4GhOoo%2BY%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"2-vyGp6PvFo4RvsFtPoIWeCReyIC8"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=88005b4fbc3d5a00-DEL

Response body is :
